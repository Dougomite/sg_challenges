
I've created a zip file of the necessary Unity scripts and scene. Unzip the files and open the 'Test' scene file under the 'Scenes' folder.

Input Format

The test data set is in the 'ages' variable of the 'AgeCheck' script:
public int[,] ages = new int[,]
	{ 
		{1914,1945},
		{1910,1935},
		{1910,1944},
		{1912,1943},
		{1912,1943},
		{1900,1943},
		{1922,2000},
		{2000,2000},
		{1999,1999},
		{2000,1900},
		{2000,4300}
	};

	
Output Format
Based on the above data set the 'AgeCheck' function will output the following to the Debug Console:
1922