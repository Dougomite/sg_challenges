<?php
function test_input($data) {
  $data = trim($data);
  $data = stripslashes($data);
  $data = htmlspecialchars($data);
  return $data;
}

if ($_SERVER["REQUEST_METHOD"] == "POST") 
{
	if (empty($_POST['hash'])) {
    $nameErr = "Hash is missing";
  } else {
    $name = test_input($_POST['hash']);
  }
 if (empty($_POST['coinsWon'])) {
    $nameErr = "Coins won is missing";
  } else {
    $name = test_input($_POST['coinsWon']);
	if($_POST["coinsWon"] < 0)
		$nameErr = "Coins won is invalid";
  }
  if (empty($_POST['coinsBet'])) {
    $nameErr = "Coins Bet is incorrect";
  } else {
    $name = test_input($_POST["coinsBet"]);
	if($_POST["coinsBet"] < 0)
		$nameErr = "Coins bet is invalid";
  }
  if (empty($_POST['playerID'])) {
    $nameErr = "ID is missing";
  } else {
    $name = test_input($_POST["playerID"]);
	if (!preg_match("/^[a-zA-Z0-9 ]*$/",$_POST["playerID"]))
		$nameErr = "ID is incorrect";
  }

$hostname =  "mysql8.000webhost.com";
$servername = "a6904416_SG1";
$username = "a6904416_douglas";
$password = "SGC_SGC_DB";

$short_connect = new mysqli($hostname, $username, $password, $servername);

// Check connection
if ($short_connect->connect_error) {
    die("Connection failed: " . mysqli_connect_error());
} 

//echo "Connected successfully. ";

//Query player
$sql = 'SELECT * FROM `Player` WHERE `playerID` = '. $_POST['playerID'];

$result = $short_connect->query($sql);

if (($result) && ($result->num_rows > 0))
{
	//echo "Found Values successfully";
	$results = array();

	//convert query result into an associative array
	while ($row = $result->fetch_assoc())
	{
		//echo "array created";
		$results[] = $row;
		
		$hash = (string) $_POST['hash'];

		//grab the salt from the db
		$salt = $row['saltValue'];

		// verify hash - HACK skipping salt check
		if (TRUE || hash('sha256', "pass ". $salt) == $hash) 
		{
			//echo "Hash match";

			//Check that user has credits to bet
			if($row['credits'] >= $_POST['coinsBet'])
			{
				//echo "Credits Available";
				$newCoins = $row['credits'] + $_POST['coinsWon'] - $_POST['coinsBet'];
				$data = array($row['playerID'], $row['name'], $newCoins, $row['lifetimeSpins'] + 1);
				$sql = 'UPDATE `Player` SET `credits`= '. $newCoins .', `lifetimeSpins`= '. ($row['lifetimeSpins'] + 1) .' WHERE `playerID` = '. $row['playerID'];	
				
				//Update player
				if ($short_connect->query($sql) === TRUE) {
					//echo "Record updated successfully";
					$json_data = array("playerID"=>$data[0], "name"=>$data[1], "credits"=>$data[2], "lifetimeSpins"=>$data[3], "lifetimeAverageReturn"=>($data[2] / $data[3]) );
					header('Content-Type: application/json');
					//return response
					echo json_encode($json_data);
				} else {
					echo "Error updating record: " . $short_connect->error;
					 die("Connection failed: " . $short_connect->error);
					 exit();
				}
			}
		}
	}

	$result->free();
}

$short_connect->close();
}
?>